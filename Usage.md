#### 可以用github账号登录
http://git.oschina.net/   国内  可以免费创建私有库

https://bitbucket.org/knight-zhou   国外 可以免费创建私有库


操作方法：
```
$ssh-keygen.exe -C "邮箱地址"  -t rsa       这里的邮箱地址就是你注册oschina的邮箱地址。(添加sshkey)
$git bash here
$git clone ....
$git add *
$ git commit -m "change"
$git push origin master
```
#### git使用教程:

```
使用 tortoisegit  非常方便(安装的时候选择git key)
方法步骤:先git clone, 然后commit  最后pull 。
如果不想每次输入密码 可以先用git shell可以更新和提交即可。图形界面就不需要每次要账号和密码
```

#### git 强行pull文件到本地

```
git fetch --all  
git reset --hard origin/master 
git pull
```

##### 模拟作host解析的三种方式
```go
curl job.huolala.cn/hll.php -x 127.0.0.1:80    // 推荐方式

curl -H "Host:job.huolala.cn" "127.0.0.1/1.php"    //

curl --resolve "www.xyz.com:80:192.168.3.2" www.xyz.com/aa.html



```
