### 分支操作
* 删除本地分支 git branch -d dev
* 删除远端分支 git push origin --delete dev

* 切换分支 git checkout dev
* 创建分支并切换 git checkout -b dev

* 建立本地分支到远程仓库的链接 -- 这样代码才能提交上去
```
git push --set-upstream origin dev   
```
* git 的分支合并到master
```
git checkout master
git merge dev
git status
git push origin master
```

### 误删除文件之后的恢复
git log   // 可以查看所有的提交日志
```
git reflog
git reset --hard c3a49c3   //退回到上一个版本,用git log则是看不出来被删除的commitid,用git reflog则可以看到被删除的commitid,我们就可以买后悔药,恢复到被删除的那个版本
```

#### 通过命令行模拟sourcetree等GUI工具
	命令行删除文件直接rm即可,commit 的时候使用: git commit -am "del"
	命令行如果使用git rm删除，就可以直接git add *,然后 git commit -m "del"
	多使用git status 查看`staged` 和 `Untracked`的状态
	
	命令行修改文件但是并没有增加文件的时候,既可以使用`git add` + `git commit -m` 也可以使用
	`git commit -am`

## git 删除带空格的文件夹
```bash
git rm -rf test\ 基础
git add .   #  记住一定要使用点好，如果用星号的话，本地删除的文件无法提交上去
git commit -m "aa"
git push
```

### git 打tag
```
还可以创建带有说明的标签，用-a指定标签名，-m指定说明文字：
$ git tag -a v0.1 -m "version 0.1 released" 3628164

删除本地标签 ：
$ git tag -d v0.1
$ git tag

$ git push origin v1.0   #推送某个标签到远程
$ git push origin --tags   #一次性推送所有的本地tag

删除远程标签:
$ git tag -d v1.0
$ git push origin :refs/tags/v1.0
```
### git stash 暂存命令
```shell
git stash save "test"  # 暂存命令
git status
git stash list    # 查看list
git stash pop stash@{0}  # 恢复指定的暂存
git stash drop ""   # 移除指定stash
git stash shown ""   # 查看指定diff
git stash -h 		#查看帮助命令
```

#### 查看object大小
```
git count-objects -v
```

#### git 清空commits历史记录

```
rm -rf .git 
git init
git add .                # 重新添加所有的文件
git commit -m "clean all then first"

git remote add origin https://knight-zhou@bitbucket.org/knight-zhou/python.git			#添加远程仓库
git remote -v    #可以查看远程仓库

git push -f origin master			#硬提交，覆盖远程仓库的commits历史记录

// 至此去页面看看

```

#### git瘦身
git 先clone下来，然后删除仓库 重新提交即可

#### 新建分支以及合并分支
```shell
		#新建分支在控制台新建
git branch            # 查看分支
git checkout -b dev     # 切换到分支
git checkout master    #切到master分支
git mege dev  # 合并dev分支
git push  # 提交
````

#### 删除分支
```go
git branch -d bug_xzx   // 删除本地的bug_xzx分支

git push origin --delete bug_xzx   //删除远程的bug_xzx分支
```

