## 如何使用

- Requires Jenkins ( jenkins-k8s.test.cn)
- 只适用于PHP7的项目发布

发布PHP7应用到K8S时，一个POD运行三个容器：

	registry-vpc.cn-shenzhen.aliyuncs.com/test_k8s/test_nginx:1.14.2
	registry-vpc.cn-shenzhen.aliyuncs.com/test_k8s/test_php:7.2.13
	registry-vpc.cn-shenzhen.aliyuncs.com/test_k8s/${proname}:版本  --代码容器，每次构建只会构建代码容器

发布需要使用到的yaml文件有：
- nginx-configmap.yaml --nginx主配置文件，通常不需要修改
- nginx-domain-configmap.conf --域名配置文件，如果项目需要用到特别的nginx配置则自行修改
- php_env-configmap.yaml --php的abc.env变量，根据不同环境使用不同的变量dev/stg/gra/prd
- deployment_to_k8s_template_php.yaml --deployment主配置，包含deployment/service/ingress，通常不需要修改

### test_php:7.2.13 模块

	[PHP Modules]
	bcmath
	Core
	ctype
	curl
	date
	dom
	fileinfo
	filter
	ftp
	gd
	hash
	iconv
	json
	libxml
	mbstring
	mongodb
	mysqli
	mysqlnd
	openssl
	pcntl
	pcre
	PDO
	pdo_mysql
	pdo_sqlite
	Phar
	posix
	rdkafka
	readline
	redis
	Reflection
	session
	SimpleXML
	sockets
	sodium
	SPL
	sqlite3
	standard
	swoole
	sysvmsg
	sysvshm
	tokenizer
	xml
	xmlreader
	xmlwriter
	zlib

### Dockerfile

	FROM busybox:1
	MAINTAINER knight.zhou
	COPY ${domain} /${domain}/
代码容器只需要把代码COPY进去即可

### deployment_to_k8s_template_php.yaml
```
变量说明
- $proname  		    要发布的项目名，example:www-abc/ucore-test
- $domain			    实际访问的域名，example:www.abc.cn/ucore.test.cn
- $replicas 		    pod的数量，
- $namespace 		    命名空间，stg为stg-php
- $php.limits.memory    pod限制php的cpu
- $php.limits.cpu 	    pod限制php的内存
- $ngx.limits.memory 	pod限制ngx的cpu
- $ngx.limits.cpu 	    pod限制ngx的内存
- $env                  php.ini里的abc.env变量,value:stg/gra/prd
```