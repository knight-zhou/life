Markdown 在线 编辑器推荐:

中文推荐:
[http://mahua.jser.me/](http://mahua.jser.me/)

英文推荐:
[http://dillinger.io/](http://dillinger.io/)

bitbucket的markdown官方demo:

[https://bitbucket.org/tutorials/markdowndemo](https://bitbucket.org/tutorials/markdowndemo)

他人参考链接:

https://bitbucket.org/FridaFlorek/coffee/src

https://bitbucket.org/mozman/xls2dxf/src

__markdown 实用语法__:

1.脚本或者程序语言块:

把程序用6个反引号包起来

2.语法高亮

将需要设置的文字两端 "``"即可

3.斜体

将需要设置为斜体的文字两端使用1个“*”或者“_”夹起来

4.粗体

将需要设置为斜体的文字两端使用2个“*”或者“_”夹起来

5.引用图片
```
![](./images/tuxing.png)
```

6. 引用文字。也可以引用的多层嵌套

   ```
   >  请问  Markdwon  怎么用？  -  小白
   >>  自己看教程！  -  愤青
   >>>  教程在哪？  -  小白
   >>  
   >>  我也不知道在哪！  -  愤青
   >
   >  那你回答个屁。  -  小白
   ```
   
   码云(gitee) 不支持 文件变红 也就是这种`<font color="red"> 这是红色字体</font>`, 可以使用如下：
   
   `<mark>这是纯黄字体</mark>` 


   