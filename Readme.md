#### pip镜像源
```
pip install redis -i http://pypi.douban.com/simple/

pip3 install flask_sqlalchemy -i https://pypi.douban.com/simple/ --trusted-host=https://pypi.douban.com/simple

```


#### windows curl socks5代理
环境变量设置:
```
set http_proxy=socks5://127.0.0.1:10808
set https_proxy=socks5://127.0.0.1:10808
```
命令行执行
```shell
curl www.google.com --socks5 127.0.0.1:10808        # s5代理方式

curl -x 127.0.0.1:8118 www.google.com      # http代理方式

```

#### vim取消首行黄色
进入vim然后使用":noh"即可


nodepad++ 写的文件在linux上用submine打开是乱码.可以用submine安装`ConvertToUTF8`和`GBK`以及`Codecs33`插件



#### 四门语言

- 写接口：php,python,Java,golang 四门语言都可以写，只是开发效率的问题。

- 渲染HTML页面： php,Python

- Golang适合写加密的服务或者后台接口服务。

- Java适合写后台接口服务或者客户端程序。